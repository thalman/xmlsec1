#!/bin/bash

export GIT_SSL_NO_VERIFY=true
git clone https://github.com/latchset/federation_testing.git

cd federation_testing
if [ ! -d /tmp/artifacts ]; then
    mkdir -p /tmp/artifacts
fi

./setup.sh
./test_xmlsec.sh
